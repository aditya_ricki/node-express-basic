const express = require('express')
const router = express.Router()
const blogController = require('../controllers/blog')

// routes index
router.get('/', blogController.index)

// routes create
router.get('/create', blogController.create)

// routes store db
router.post('/', blogController.store)

// routes details
router.get('/:id', blogController.show)

// routes delete
router.delete('/:id', blogController.destroy)

module.exports = router