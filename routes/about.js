const express = require('express')
const router = express.Router()
const aboutController = require('../controllers/about')

// routes index
router.get('/', aboutController.index)

module.exports = router