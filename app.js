const express = require('express')
const morgan = require('morgan')
const mongoose = require('mongoose')
const blogRoutes = require('./routes/blog')
const aboutRoutes = require('./routes/about')

const app = express()

// db connection
const dbURI = 'mongodb+srv://msdcode:melasd2205@node-tutorial-netninja.oevnz.mongodb.net/node-netninja?retryWrites=true&w=majority'
// jika parameter kedua tidak ada maka akan ada warning
mongoose.connect(dbURI, {
	useNewUrlParser: true,
	useUnifiedTopology: true
}).then(result => app.listen(3000))
.catch(err => console.log(err))

// set view engine ejs
app.set('view engine', 'ejs')
// jika folder views namanya bukan views
// app.set('views', 'nama folder')

// listening port 3000 dipindah saat berhasil konek ke db

// middleware & static files
// membuat semua file yang ada didalam folder public bisa diakses secara langsung dari url
// sehingga file style.css di folder public dapat diakses di partials/header.ejs secara langsung
app.use(express.static('public'))
app.use(express.urlencoded({ extended: true }))
app.use(morgan('dev'))

// routes
app.get('', (req, res) => {
	res.redirect('/blogs')
})

app.get('/about-us', (req, res) => {
	res.redirect('/about')
})

// about routes
app.use('/about', aboutRoutes)

// blog routes
app.use('/blogs', blogRoutes)

app.use((req, res) => {
	res.render('404', {
		title: '404'
	})
})