const Blog = require('../models/blog')

// index
const index = (req, res) => {
	Blog.find().sort({ createdAt: 'desc' })
	.then(result => {
		res.render('blogs/index', {
			title: 'HOME',
			blogs: result
		})
	})
	.catch(err => console.log(err))
}

const create = (req, res) => {
	res.render('blogs/create', {
		title: 'CREATE'
	})
}

const store = (req, res) => {
	const blog = new Blog({
		title: req.body.title,
		snippet: req.body.snippet,
		body: req.body.body
	})

	blog.save()
	.then(result => res.redirect('/blogs'))
	.catch(err => console.log(err))
}

const show = (req, res) => {
	const id = req.params.id

	Blog.findById(id)
	.then(result => {
		res.render('blogs/details', {
			title: 'Details Blog',
			blog: result
		})
	})
	.catch(err => {
		res.status(404).render('404', {
			title: 'Blog not Found'
		})
	})
}

const destroy = (req, res) => {
	const id = req.params.id

	Blog.findByIdAndDelete(id)
	.then(result => res.json({ redirect: '/blogs' }))
	.catch(err => console.log(err))
}

module.exports = {
	index,
	create,
	store,
	show,
	destroy
}