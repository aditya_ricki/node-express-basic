const index = (req, res) => {
	res.render('about', {
		title: 'ABOUT'
	})
}

module.exports = {
	index
}